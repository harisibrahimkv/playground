from fabric import Connection, task

localhost = Connection(
    'localhost',
    user="haris",
    connect_kwargs={'key_filename': '/home/haris/.ssh/id_rsa'}
)


@task
def hello_world(c):
    localhost.run('uname -s')
